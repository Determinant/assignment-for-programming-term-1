#include "st02.h"
#include <algorithm>
#include <ctime>
#include <cstdio>
#include <cstdlib>

using namespace std;

struct ar
{
};

void st02::init()
{
	for ( int i = 0 ; i < 3 ; i ++ ) increaseHealth();
	for ( int i = 0 ; i < 6 ; i ++ ) increaseSpeed();
	increaseStrength();
}

#define dis( a , b , c , d ) ( abs( a - c ) + abs( b - d ) )

void st02::play()
{
	srand(time(0));
	static int Round = 0;
	static int MaxHp[ MAX_PLAYER + 10 ];
	static int MaxSpeed[ MAX_PLAYER + 10 ];
	static int LastX[ MAX_PLAYER + 10 ];
	static int LastY[ MAX_PLAYER + 10 ];
	if ( ! Round )
	{
		memset( MaxHp , 0 , sizeof( MaxHp ) );
		memset( MaxSpeed , 0 , sizeof( MaxSpeed ) );
		memset( LastX , 0 , sizeof( LastX ) );
		memset( LastY , 0 , sizeof( LastY ) );
	}
	Round ++;
	static bool IsLive[ MAX_PLAYER + 10 ];
	static int qs[ MAX_PLAYER + 10 ];
	memset( IsLive , 0 , sizeof( IsLive ) );
	for ( int i = 1 ; i <= getTotalPlayer() ; i ++ )
	{
		MaxHp[ i ] = max( MaxHp[ i ] , askHP( i ) );
		qs[ i ] = askHP( i );
	}
	sort( qs + 1 , qs + getTotalPlayer() + 1 );
	for ( int i = 1 ; i <= N ; i ++ )
		for ( int j = 1 ; j <= M ; j ++ )
		{
			LastMap[ i ][ j ] = askWhat( i , j );
			if ( askWhat( i , j ) > 0 )
			{
				int t = askWhat( i , j );
				if ( LastX[ t ] != 0 )
					MaxSpeed[ t ] = max( MaxSpeed[ t ]  , dis( i , j , LastX[ t ] , LastY[ t ] ) / 2 );
				LastX[ t ] = i;
				LastY[ t ] = j;
				IsLive[ t ] = 1;
			}
		}
	for ( int i = 1 ; i <= getTotalPlayer() ; i ++ )
		if ( !IsLive[ i ] )
		{
			LastX[ i ] = 0;
			LastY[ i ] = 0;
		}
	int x = 0 , y = 0;
	for ( int i = 1 ; i <= N ; i ++ )
		for ( int j = 1 ; j <= M ; j ++ )
			if ( askWhat( i , j ) > 0 && dis( i , j , getX() , getY() ) <= getSp() + 1 && getPoint() / 2 + getAtt() >= askHP( askWhat( i , j ) ) )
			{
				bool flag = 0;
				if ( x > 1 && askWhat( x - 1 , y ) == EMPTY ) flag = 1;
				if ( x < N && askWhat( x + 1 , y ) == EMPTY ) flag = 1;
				if ( y > 1 && askWhat( x , y - 1 ) == EMPTY ) flag = 1;
				if ( y < M && askWhat( x , y + 1 ) == EMPTY ) flag = 1;
				if ( !flag ) continue;
				if ( x == 0 )
				{
					x = i;
					y = j;
				}
				if ( MaxHp[ askWhat( x , y ) ] + MaxSpeed[ askWhat( x , y ) ] < MaxHp[ askWhat( i , j ) ] + MaxSpeed[ askWhat( i , j ) ] )
				{
					x = i;
					y = j;
				}
			}
	if ( x != 0 )
	{
		move( x - 1 , y );
		move( x + 1 , y );
		move( x , y - 1 );
		move( x , y + 1 );
		while ( getAtt() < askHP( askWhat( x , y ) ) && getPoint() ) increaseStrength();
		attack( x , y );
	}
	else
	{
		for ( int i = 1 ; i <= N ; i ++ )
			for ( int j = 1 ; j <= M ; j ++ )
			if ( askWhat( i , j ) == FOOD ) 
			{
				bool flag = 0;
				if ( x > 1 && askWhat( x - 1 , y ) == EMPTY ) flag = 1;
				if ( x < N && askWhat( x + 1 , y ) == EMPTY ) flag = 1;
				if ( y > 1 && askWhat( x , y - 1 ) == EMPTY ) flag = 1;
				if ( y < M && askWhat( x , y + 1 ) == EMPTY ) flag = 1;
				if ( !flag ) continue;
				if ( x == 0 )
				{
					x = i;
					y = j;
					continue;
				}
				if ( dis( i , j , getX() , getY() ) < dis( x , y , getX() , getY() ) )
				{
					x = i;
					y = j;
				}
			}
		move( x - 1 , y );
		move( x + 1 , y );
		move( x , y - 1 );
		move( x , y + 1 );
		attack( x , y );
		int p = getX() , q = getY();
		for ( int i = 1 ; i <= N ; i ++ )
			for ( int j = 1 ; j <= M ; j ++ )
				if ( askWhat( i , j ) == EMPTY )
				if ( dis( i , j , getX() , getY() ) <= getSp() )
					if ( dis( i , j , x , y ) < dis( p , q , x , y ) )
					{
						p = i;
						q = j;
					}
		move( p , q );
	}
	if (getLevel()>30)
	{
		int pp = rand() % 3;
		if ( pp== 1  ) increaseSpeed();
		if (pp == 0 )increaseStrength();
		if (pp==2)increaseHealth();
	}
	while ( getPoint() && getSp() < 20 ) {
		increaseSpeed();
		increaseSpeed();
		increaseHealth();
	}
	int pqr = getPoint();
	if ( pqr >= getLevel() )
	for ( int i = 1 ; i <= pqr / 3 ; i ++ ) increaseSpeed();
	for ( int i = 1 ; i <= N ; i ++ )
		for ( int j = 1 ; j <= M ; j ++ )
			LastMap[ i ][ j ] = askWhat( i , j );
	int ttt = 0;
	while ( getHP() < qs[ getTotalPlayer() / 3 ] && getPoint() && ttt * 3 <= getPoint() ) 
	{
		increaseHealth();
		ttt ++;
	}
	for ( int i = 1 ; i <= getTotalPlayer() ; i ++ )
		qs[ i ] = MaxSpeed[ i ];
	sort( qs + 1 , qs + getTotalPlayer() + 1 );
	ttt = 0;
	while ( getSp() < 80 && getSp() < qs[ getTotalPlayer() / 2 ] && getPoint() && ttt * 3 <= getPoint() )
	{
		increaseSpeed();
		ttt ++;
	}
}


int st02::calc( int x , int y )
{
	int cnt = 0;
	bool flag = 1;
	for ( int i = 1 ; i <= N ; i ++ )
		for ( int j = 1 ; j <= M ; j ++ )
			if ( LastMap[ i ][ j ] == FOOD && dis( i , j , x , y ) <= getSp() )
				cnt += 5;
	return cnt;	
}

void st02::revive( int &x , int &y )
{
	
	x = y = 1;
	for ( int i = 1 ; i <= N ; i ++ )
		for ( int j = 1 ; j <= M ; j ++ )
			if ( LastMap[ i ][ j ] == EMPTY )
			if ( calc( i , j ) > calc( x , y ) )
			{
				x = i;
				y = j;
			}
	int t = getPoint();
	for ( int i = 1 ; i <= t / 3 ; i ++ ) increaseHealth();
	for ( int i = 1 ; i <= t / 3 ; i ++ ) increaseSpeed();
}

