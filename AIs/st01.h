#ifndef AI_DET_H
#define AI_DET_H

#include "fish.h"


namespace AIDetFunctional {

    const double EPS = 1e-8;
    const double INF = 1e50;

    const int MAX_LEVEL = 1000;
    const int MAP_MAXR = 40;
    const int MAP_MAXC = 40;
    const int MAX_PLAYER = 40;

    const int PLACE_TYPE_BLANK = 0;
    const int PLACE_TYPE_FOOD = 1;
    const int PLACE_TYPE_INVALID = 0x7fffffff;

    const int PLAYER_MAX_SPEED = MAP_MAXR - 1 + MAP_MAXC - 1;
    const int PLAYER_INIT_POINT = 10;

    const int PLAYER_STATUS_REVIVING = -1;
    const int PLAYER_STATUS_ALIVE = 1;
    const int PLAYER_STATUS_DEAD = 0;

    const int MOVE_TYPE_AGGRESSIVE = 1;
    const int MOVE_TYPE_PEACEFUL = 0;

    const int LEVEL_POINT = 3;

    const int STRATEGY_TYPE_ATTACK = 0;
    const int STRATEGY_TYPE_MOVE = 1;

    const int MONTE_CARLO_ITER_TIMES = 10;

    const int dir[4][2] = {{-1, 0}, {0, -1}, {1, 0}, {0, 1}};

    typedef int const (*Map_t)[MAP_MAXC];

    class AIDet;
    class AIState;

    struct Pos {
        int x, y;
        Pos();
        Pos(int x, int y);
        bool empty() const;
    }; // the paired int to represent the location in a map

    struct Strategy {
        Pos dpos, apos;
        int type;
        double eval;
        Strategy();
        Strategy(Pos dpos, Pos apos, double eval);
        bool operator<(const Strategy &b) const;
    };

    struct AIDummy {

        int pos_x, pos_y, id;
        int strength, speed;
        int max_health, cur_health;
        int point, exp, level, jump_sum;
        int status;  // reviving, dead or alive

        AIState *env;

        void print();
        void level_up();
        int get_score() const;
        void ai_play(int move_type);
        double evaluate() const;
    };
    class AIDataBase {

        private:
            Pos last_pos[MAX_PLAYER];
            AIDummy player_info[MAX_PLAYER];
            int map[MAP_MAXR][MAP_MAXC];
            double food_density[MAP_MAXR][MAP_MAXC];
            int player_cnt, round_cnt;
            int my_id, round_hurt;
            AIDet *fptr;

            void clean_up();
            AIDummy guess_info(int id);

        public:
            AIDataBase(AIDet *_fptr);
            AIDummy get_info(int id) const;
            Map_t get_map() const;
            void update();    
            void round_refresh();
            void round_add();
            double get_average_health() const;
            double get_average_speed() const;
            int get_round_cnt() const;
            int get_total_player() const;
            int get_my_id() const;
            double get_food_density(int x, int y) const;
    };
    // to store some statistical information

    class AIState {

        friend class AIDummy;

        private:

        int map[MAP_MAXR][MAP_MAXC];
        int player_cnt, turn_cnt;
        int my_id;
        AIDummy players[MAX_PLAYER];

        Pos random_blank();
        bool check_blank(int x, int y);
        bool check_alive(AIDummy *fptr);
        void place_fish(AIDummy *player, int x, int y);
        void clear_position(int x, int y);
        void refresh_food();

        public:

        AIState();
        AIState &operator=(const AIState &ori);
        AIState(const AIDataBase *db);
        ~AIState();

        AIDummy *get_player_ctl(int id);
        bool inside_map(int x, int y);
        void add_player(const AIDummy &ptr, int id);
        void attack(AIDummy *fptr, int x, int y);
        void move(AIDummy *fptr, int x, int y);
        void turn(int move_type);
        void monte_carlo();
        int get_content(int x, int y);
        double evaluate();
    };


    class AIDet : public fish {

        private:
            AIDataBase db;
            Strategy get_strategy();
            void allocate_point();

        public:
            AIDet();
            void init();
            void play();
            void revive(int &x, int &y);
    };

}

typedef AIDetFunctional::AIDet st01;

#endif
