#include <algorithm>
#include "fish.h"

typedef fish Fish;

const int MAX_N = N;
const int MAX_M = M;
const int MOVE[4][2] = {{0, -1}, {0, 1}, {-1, 0}, {1, 0}};
const double EPS = 1e-8;

const int INF = (int)1e9;
const int INVALID = -999;
const int KILL_BOUND = 100;

const double HP_KEY = 2.;
const double ATTACK_KEY = 1.;
const double SPEED_KEY = 2.;

const int MAX_ATTACK = 100;

const int MAX_SPEED = std::max(MAX_N, MAX_M) * 2 - 2;

const int HIDE_RANDOM_CONTROL = 5;

struct Point {
	int x, y;
	Point() {}
	Point(int x, int y): x(x), y(y) {}
	bool operator <(const Point &) const;
};

int sgn(double, double);

int manhattonDist(const Point &, const Point &);

bool isValid(int, int);

class st03:public Fish {
	private:
		int intervalRandom(int, int);
		int dist(int, int);
		void assignPoints();
		int round;
	public:
		st03();
		void init();
		void play();
		void revive(int &, int &);
};

