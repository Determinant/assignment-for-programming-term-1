#include "st01.h"
#include <algorithm>
#include <cmath>
#include <cstring>
#include <cassert>
#include <cstdio>
#include <cstdlib>

using std::max;
using std::min;
using std::random_shuffle;
using std::sort;

namespace AIDetFunctional {


    Pos::Pos() {}
    Pos::Pos(int _x, int _y) : x(_x), y(_y) {}
    bool Pos::empty() const {
        return x == -1 && y == -1;
    }

    Strategy::Strategy() {}
    Strategy::Strategy(Pos _dpos, Pos _apos, double _eval) : 
        dpos(_dpos), apos(_apos), eval(_eval) {}
    bool Strategy::operator<(const Strategy &b) const {
        return eval < b.eval;
    }


    //////////////       AIState Implementation      //////////////

    void AIState::clear_position(int x, int y) {
        map[x][y] = PLACE_TYPE_BLANK;
    }

    void AIState::place_fish(AIDummy *player, int x, int y) {
        player -> pos_x = x;
        player -> pos_y = y;
        map[x][y] = -(player -> id + 1);
    }

    bool AIState::check_alive(AIDummy *fptr) {
        if (fptr -> cur_health > 0) return true; // still alive

        clear_position(fptr -> pos_x, fptr -> pos_y);
        fptr -> status = PLAYER_STATUS_DEAD;
        return false;
    }

    void AIState::attack(AIDummy *fptr, int x, int y) {

        if (map[x][y] == PLACE_TYPE_FOOD)
        {
            int max_hp = fptr -> max_health;
            int cur_hp = fptr -> cur_health;
            fptr -> cur_health = min(max_hp, cur_hp + max(2, max_hp / 10));
            fptr -> exp += 1;
            clear_position(x, y);
        }
        else
        {
            AIDummy *opp = players + (-map[x][y] - 1);
            assert(-player_cnt <= map[x][y] && map[x][y] <= -1);
            opp -> cur_health -= fptr -> strength;
            if (!check_alive(opp) && fptr -> level < opp -> level)
                fptr -> jump_sum += opp -> level - fptr -> level;
            fptr -> exp += max(1, opp -> level / 2);
        }
        fptr -> level_up();
    }

    void AIState::move(AIDummy *fptr, int new_x, int new_y) {
        int from_x = fptr -> pos_x, from_y = fptr -> pos_y;

        clear_position(fptr -> pos_x, fptr -> pos_y);
        place_fish(fptr, new_x, new_y);
    }

    void AIState::refresh_food() {
        static Pos avail[MAP_MAXR * MAP_MAXC];
        int acnt = 0;
        for (int i = 0; i < MAP_MAXR; i++)
            for (int j = 0; j < MAP_MAXC; j++)
            {
                if (map[i][j] < 0) continue;
                if (map[i][j] == PLACE_TYPE_FOOD) 
                    map[i][j] = PLACE_TYPE_BLANK;
                avail[acnt++] = Pos(i, j);
            }

        random_shuffle(avail, avail + acnt);
        int size = min(MAX_FOOD, acnt);
        for (int i = 0; i < size; i++)
            map[avail[i].x][avail[i].y] = PLACE_TYPE_FOOD;
    }

    Pos AIState::random_blank() {
        int x, y;
        do x = rand() % MAP_MAXR, 
            y = rand() % MAP_MAXC;
        while (map[x][y] != PLACE_TYPE_BLANK);
        return Pos(x, y);
    }

    void AIState::add_player(const AIDummy &ref, int id) {
        players[id] = ref;
        players[id].env = this;
    }

    int dcmp(double x) {
        if (fabs(x) < EPS) return 0;
        return x < 0 ? -1 : 1;
    }

    bool cmp(const AIDummy *a, const AIDummy *b) {
        if (a -> speed != b -> speed) 
            return a -> speed > b -> speed;
        double rate_a = a -> cur_health / (double)a -> max_health;
        double rate_b = b -> cur_health / (double)b -> max_health;
        int sgn = dcmp(rate_a - rate_b);
        if (sgn != 0) return sgn > 0;
        return a -> get_score() < b -> get_score();
    }

    void AIState::turn(int move_type) {

        static AIDummy *player_list[MAX_PLAYER];
        for (int i = 0; i < player_cnt; i++)
            player_list[i] = players + i;

        sort(player_list, player_list + player_cnt, cmp);

        if (turn_cnt % FOOD_ROUND == 0) refresh_food();
        for (int i = 0; i < player_cnt; i++)
        {
            AIDummy *player = player_list[i];
            if (player -> status == PLAYER_STATUS_REVIVING)
            {
                // it's time to revive!
                int x, y;
                Pos p = random_blank();
                x = p.x;
                y = p.y;
                place_fish(player, x, y);
                player -> status = PLAYER_STATUS_ALIVE;
                // becomes alive again
            }
        }

        for (int i = 0; i < player_cnt; i++)
        {
            /*            for (int j = 0; j < 2; j++)
                          players[j].print();
                          puts("=========");
                          */
            AIDummy *player = player_list[i];
            if (player -> status == PLAYER_STATUS_ALIVE)
            {
                player -> ai_play(move_type);
            }
            else if (player -> status == PLAYER_STATUS_DEAD)
                player -> status = PLAYER_STATUS_REVIVING;
            //the dead player will revive in the next turn
        }

        turn_cnt++;
    }

    bool AIState::inside_map(int x, int y) {
        return (0 <= x && x < MAP_MAXR &&
                0 <= y && y < MAP_MAXC);
    }

    AIState::AIState() {}
    AIState &AIState::operator=(const AIState &ori) {
        memmove(map, ori.map, sizeof map);
        player_cnt = ori.player_cnt;
        turn_cnt = ori.turn_cnt;
        my_id = ori.my_id;
        for (int i = 0; i < player_cnt; i++)
        {
            players[i] = ori.players[i];
            players[i].env = this;
        }
        return *this;
    }
    AIState::AIState(const AIDataBase *db) {
        memmove(map, db -> get_map(), sizeof map);
        player_cnt = db -> get_total_player();
        turn_cnt = db -> get_round_cnt();
        // copy the map
        my_id = db -> get_my_id();
        for (int i = 0; i < player_cnt; i++)
            add_player(db -> get_info(i), i);
    }

    AIState::~AIState() {
    }

    double AIState::evaluate() {
        double others = 0, me = 0;
        for (int i = 0; i < player_cnt; i++)
            if (i == my_id) me = players[i].evaluate();
            else others += players[i].evaluate();
        return me; // - others / double(player_cnt - 1) * 0.1;
    }

    AIDummy *AIState::get_player_ctl(int id) {
        return players + id;
    }

    void AIState::monte_carlo() {
        for (int i = 0; i < MONTE_CARLO_ITER_TIMES; i++)
            turn(MOVE_TYPE_PEACEFUL);
    }

    int AIState::get_content(int x, int y) {
        return map[x][y];
    }

    ////////////////////////

    //////////////       AIDataBase Implementation      //////////////

    double AIDataBase::get_food_density(int x, int y) const {
        return food_density[x][y];
    }

    bool lt_cmp(int a, int b) { return a > b; }
    double AIDataBase::get_average_health() const {
        static int hp[MAX_PLAYER];
        for (int i = 0; i < player_cnt; i++)
            hp[i] = player_info[i].cur_health;
        sort(hp, hp + player_cnt, lt_cmp);
        //        fprintf(stderr, "%d\n", hp[player_cnt / 3]);
        return hp[player_cnt / 2];
    }
	
    double AIDataBase::get_average_speed() const {
        static int sp[MAX_PLAYER];
        for (int i = 0; i < player_cnt; i++)
            sp[i] = player_info[i].speed;
        sort(sp, sp + player_cnt, lt_cmp);
        return sp[rand() % 5];
    }

    AIDataBase::AIDataBase(AIDet *_fptr) : fptr(_fptr) {

        for (int i = 0; i < MAX_PLAYER; i++)
            last_pos[i] = Pos(-1, -1);
        round_hurt = -1;
        //        player_cnt = fptr -> ask;
    }

    void AIDataBase::round_refresh() { round_cnt = 0; }
    void AIDataBase::round_add() { round_cnt++; }
    int AIDataBase::get_round_cnt() const { return round_cnt; }

    int AIDataBase::get_total_player() const {
        return player_cnt;
    }

    void AIDataBase::clean_up() {

        for (int i = 0; i < player_cnt; i++)
        {
            AIDummy *player = player_info + i;
            last_pos[i] = Pos(player -> pos_x, player -> pos_y);
        }
        round_hurt = 0;
    }

    AIDummy AIDataBase::guess_info(int id) {

        AIDummy res = player_info[id];
        AIDummy *me = player_info + my_id;

        const double strength_del_rate = 0.2;
        const double speed_coef = 1.2;
        const double health_coef = 1.5;

        int rnd_range = max(int(me -> strength * strength_del_rate), 1);
        res.strength = max(me -> strength + rnd_range - rand() % (rnd_range * 2), 1) * 1.5;
        // guess the strength

        if (last_pos[id].empty())
        {
            rnd_range = max(int(me -> speed * speed_coef), 1);
            res.speed = me -> speed + rnd_range - rand() % (rnd_range * 2);
        }
        else
        {
            res.speed = min(
                    int((abs(res.pos_x - last_pos[id].x) +
                            abs(res.pos_y - last_pos[id].y)) * speed_coef), PLAYER_MAX_SPEED);
        }
        // guess the speed

        res.max_health = max(int(res.cur_health * health_coef), me -> max_health);

        // guess the max_health

        res.point = 0; // assume that the player has no points
        res.jump_sum = 0;

        double exp_coef = me -> exp / double(me -> strength + me -> speed + me -> max_health + 1);
        res.exp = (res.strength + res.speed + res.max_health) * exp_coef;
        // guess the exp
        res.level = 1;
        res.level_up();
        // caculate the level
        //      res.print();
        return res;
    }

    void AIDataBase::update() {

        round_add();
        player_cnt = fptr -> getTotalPlayer();
        // get the total number of players
        for (int i = 0; i < player_cnt; i++)
            player_info[i].id = -1;
        for (int i = 0; i < MAP_MAXR; i++)
            for (int j = 0; j < MAP_MAXC; j++)
            {
                int cell = fptr -> askWhat(i + 1, j + 1);
                if (cell == EMPTY) map[i][j] = PLACE_TYPE_BLANK;
                else if (cell == FOOD) map[i][j] = PLACE_TYPE_FOOD;
                else 
                {
                    int id = cell - 1;
                    map[i][j] = -cell;
                    AIDummy *player = player_info + id;
                    player -> pos_x = i;
                    player -> pos_y = j;
                    player -> cur_health = fptr -> askHP(cell);
                    player -> id = id;
                    player -> status = PLAYER_STATUS_ALIVE;
                }
            }

        for (int i = 0; i < player_cnt; i++)
        {
            AIDummy *player = player_info + i;
            if (player -> id == -1)
            {
                player -> cur_health = 0;
                player -> id = i;
                player -> status = PLAYER_STATUS_REVIVING;
            }
        }
        // obtain the map

        my_id = fptr -> getID() - 1; // who am I ?
        AIDummy *me = player_info + my_id; 
        me -> pos_x = fptr -> getX() - 1;
        me -> pos_y = fptr -> getY() - 1;
        me -> id = my_id;
        me -> strength = fptr -> getAtt();
        me -> speed = fptr -> getSp();
        me -> max_health = fptr -> getMaxHP();
        me -> cur_health = fptr -> getHP();
        me -> point = fptr -> getPoint();
        me -> exp = fptr -> getExp();
        me -> level = fptr -> getLevel();

        for (int i = 0; i < player_cnt; i++) 
            if (i != my_id) player_info[i] = guess_info(i);

        for (int i = 0; i < MAP_MAXR; i++)
            for (int j = 0; j < MAP_MAXC; j++)
            {
                double &ref = food_density[i][j];
                for (int x = 0; x < MAP_MAXR; x++)
                    for (int y = 0; y < MAP_MAXC; y++)
                        ref += (map[x][y] == PLACE_TYPE_FOOD) / (double(abs(x - i) + abs(y - j)) + 1);
            }

        clean_up();
    }

    AIDummy AIDataBase::get_info(int id) const {
        // provide with some real properties of players while the others are guessed
        return player_info[id];
    }
    Map_t AIDataBase::get_map() const { return map; }

    int AIDataBase::get_my_id() const { return my_id; }

    ////////////////////////


    /////////////   AIDummy /////////////////

    double AIDummy::evaluate() const {
        return get_score() * 5;
    }

    void AIDummy::print() {
        fprintf(stderr, "id: %d\n", id);
        fprintf(stderr, "strength: %d\n", strength);
        fprintf(stderr, "speed: %d\n", speed);
        fprintf(stderr, "max_health: %d\n", max_health);
        fprintf(stderr, "cur_health: %d\n", cur_health);
        fprintf(stderr, "point: %d\n", point);
        fprintf(stderr, "exp: %d\n", exp);
        fprintf(stderr, "level: %d\n", level);
        fprintf(stderr, "jump_sum: %d\n", jump_sum);
        fprintf(stderr, "status: %d\n", status);
    }

    void AIDummy::level_up() {
        int new_level; 
        int l, r, mid;
        for (l = 1, r = MAX_LEVEL; mid = (l + r) >> 1, r - l > 1;
                (((mid + 2) * (mid - 1) / 2 <= exp) ? l : r) = mid);
        new_level = l;

        assert(new_level >= level);
        point += (new_level - level) * LEVEL_POINT;
        level = new_level;
    }

    int AIDummy::get_score() const {
        return exp + 2 * jump_sum;
    }

    void AIDummy::ai_play(int move_type) {
        if (move_type == MOVE_TYPE_PEACEFUL)
        {
            static int avail_attack[4];
            int acnt = 0;
            for (int i = 0; i < 4; i++)
            {
                int des_x = pos_x + dir[i][0],
                    des_y = pos_y + dir[i][1];
                if (!env -> inside_map(des_x, des_y)) continue;
                if (env -> get_content(des_x, des_y) != PLACE_TYPE_BLANK)
                    avail_attack[acnt++] = i;
            }
            if (acnt)
            {
                int rnd_dir = rand() % acnt;
                int des_x = pos_x + dir[avail_attack[rnd_dir]][0],
                    des_y = pos_y + dir[avail_attack[rnd_dir]][1];
                env -> attack(this, des_x, des_y);
            }
            else
            {
                int fixed_speed = min(speed, PLAYER_MAX_SPEED);
                int des_x = 0, des_y = 0;
                do 
                {
                    des_x = pos_x + fixed_speed - rand() % (fixed_speed * 2 + 1);
                    des_y = pos_y + fixed_speed - rand() % (fixed_speed * 2 + 1);
                }
                while (!env -> inside_map(des_x, des_y) || 
                        abs(des_x - pos_x) + abs(des_y - pos_y) > fixed_speed);

                int cell = env -> get_content(des_x, des_y);
                if (cell == PLACE_TYPE_BLANK)
                    env -> move(this, des_x, des_y);
            }
        }
    }

    ////////////////////////

    AIDet::AIDet() : db(this) {

    }

    void AIDet::allocate_point() {
        const double strength_rate = 1;
        const double speed_rate = 2;
        const double health_rate = 1.8;
        const double health_shreshold = 0.7;

        int point = getPoint();
        int rem = point;
        if (getSp() >= PLAYER_MAX_SPEED)
        {
            while (point)
            {
                if (point) { increaseHealth(), point--; }
                if (point) { increaseStrength(), point--; }
            }
        }
        else
        {
            int cur_health = getHP(),
                max_health = getMaxHP(),
                strength = getAtt(),
                speed = getSp();
/*
            fprintf(stderr, "%d %d %lf\n", cur_health, max_health, double(cur_health) / double(max_health));
            fprintf(stderr, "%d %lf\n", speed, db.get_average_speed());
            */
            //usleep(10000);

			while (point && (speed < db.get_average_speed()))
				increaseSpeed(), point--;

			while (point && (double(cur_health) / double(max_health) < health_shreshold 
						|| (cur_health < db.get_average_health() && rand() % 4)))
            {
//                fprintf(stderr, "%d < %lf\n", cur_health, db.get_average_health());
                increaseHealth(), point--;
            }

			if (speed >= db.get_average_speed())
            {
                while (point &&
                        (double(strength) / double (speed) < strength_rate / speed_rate ||
                         double(strength) / double (max_health) < strength_rate / health_rate))
                {
                    //printf("%d %d %d\n", strength < double(cur_health) / 5, double(strength) / double (speed) < strength_rate / speed_rate,
                    //double(strength) / double (max_health) < strength_rate / health_rate);
                    increaseStrength(), point--;
                }

                while (point &&
                        (strength < double(cur_health) / 5))
                {
                    //printf("%d %d %d\n", strength < double(cur_health) / 5, double(strength) / double (speed) < strength_rate / speed_rate,
                    //double(strength) / double (max_health) < strength_rate / health_rate);
                    increaseStrength(), point--;
                }
            }

            while (point && 
                    (double(speed) / double (strength) < speed_rate / strength_rate ||
                     double(speed) / double (max_health) < speed_rate / health_rate))
                increaseSpeed(), point--;

            while (point) increaseSpeed(), point--;
            //            printf("%d\n", getPoint());
        }
    }

    void AIDet::init() {
        for (int i = 0; i < 4; i++)
            increaseHealth();
        for (int i = 0; i < 5; i++)
            increaseSpeed();
        increaseStrength();
        db.round_refresh();
    }

    void AIDet::play() {
        db.update();
        allocate_point();
        int move_x, move_y;
        int attack_x, attack_y;

        Strategy stgy = get_strategy();
        move(stgy.dpos.x + 1, stgy.dpos.y + 1);
        attack(stgy.apos.x + 1, stgy.apos.y + 1);
    }

    void AIDet::revive(int &x, int &y) {
        allocate_point();
        db.round_add();
        x = rand() % MAP_MAXR + 1;
        y = rand() % MAP_MAXC + 1;
    }

    Strategy AIDet::get_strategy() {
        AIState cur_state = AIState(&db);
        // init the current state

        int my_id = db.get_my_id();
        AIDummy me = db.get_info(my_id);
        Strategy res(Pos(me.pos_x, me.pos_y), Pos(0, 0), -INF);

        static AIState trial;

        int fixed_speed = min(me.speed, PLAYER_MAX_SPEED);
        for (int dx = -fixed_speed; dx <= fixed_speed; dx++)
            for (int dy = -fixed_speed; dy <= fixed_speed; dy++)
                if (abs(dx) + abs(dy) <= fixed_speed)
                {
                    int des_x = me.pos_x + dx,
                        des_y = me.pos_y + dy;
                    if (!cur_state.inside_map(des_x, des_y)) continue;
                    Pos dpos(des_x, des_y);
                    int cell = db.get_map()[des_x][des_y];
                    if (cell == PLACE_TYPE_BLANK || cell == -(my_id + 1))
                    {
                        for (int d = 0; d < 4; d++)
                        {
                            double eval = 0;
                            trial = cur_state; // make a copy
                            AIDummy *t_me = trial.get_player_ctl(db.get_my_id());
                            trial.move(t_me, des_x, des_y);

                            int att_x = des_x + dir[d][0],
                                att_y = des_y + dir[d][1];
                            Pos apos(att_x, att_y);
                            if (trial.inside_map(att_x, att_y) && trial.get_content(att_x, att_y) != PLACE_TYPE_BLANK)
                            {
                                if (trial.get_content(att_x, att_y) == PLACE_TYPE_FOOD)
                                    eval += INF * (rand() % 10 + 1) * (db.get_round_cnt() < 50 ? 1000 : 1);
                                else
                                {
                                    AIDummy *opp = trial.get_player_ctl(-trial.get_content(att_x, att_y) - 1);
                                    if (opp -> cur_health <= me.strength)
                                    {
                                        /*printf("%d %d\n", att_x, att_y);
                                        opp -> print();
                                        printf("%d\n", me.strength);
                                        puts("=========");
                                        */
                                        eval += 100 * INF * (rand() % 10 + 1);
                                    }
                                    eval += max(100000.0 - (50000 * (double(opp -> cur_health - me.strength) / me.strength)), 0.0);
                                }
                                trial.attack(t_me, att_x, att_y);
                            }

                            trial.monte_carlo();
                            double param1 = trial.evaluate();
                            double param2 = db.get_food_density(des_x, des_y) * 15;
                            double param3 = (abs(dx) + abs(dy)) * 40 / double(me.speed);
                            eval += param1 + param2 + param3;
//                            fprintf(stderr, "%d %d, eval: %lf, trial.eval: %lf food_density: %lf\n", dx, dy, eval, param1, param2);
                            if (eval > res.eval) res = Strategy(dpos, apos, eval);
                        }
                    }
//                   fprintf(stderr, "--- %d, %d ---\n", dx, dy);
                }
//        fprintf(stderr, "===========================\n");
        int t = cur_state.get_content(res.apos.x,res.apos.y);

        /*fprintf(stderr, "I thought it's supposed to be %s\n", t == PLACE_TYPE_FOOD ? "food" : (t == PLACE_TYPE_BLANK ? "blank" : "player"));
          if (t < 0) fprintf(stderr, "I supposed you're gonna die %d(%d) %d\n", -t, cur_state.get_player_ctl(-t - 1) -> cur_health, me.strength);
          fprintf(stderr, "%d %d %d %d %lf\n", res.dpos.x, res.dpos.y, res.apos.x, res.apos.y, res.eval);
          */
        return res;
    }

}
