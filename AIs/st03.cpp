#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <cassert>
#include <sstream>
#include <numeric>
#include <climits>
#include <string>
#include <cctype>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <vector>
#include <queue>
#include <list>
#include <map>
#include <set>
#include "fish.h"
#include "st03.h"
using std::sort;
using std::vector;
using std::pair;
using std::make_pair;
using std::max;
using std::min;
using std::pow;

// signum function
int sgn(double a, double b) {
	return a - b < -EPS ? -1 : a - b > EPS;
}

// manhatton dist betweent two points
int manhattonDist(const Point &a, const Point &b) {
	return abs(a.x - b.x) + abs(a.y - b.y);
}

// check if the position is ok
bool isValid(int x, int y) {
	return 1 <= x && x <= MAX_N && 1 <= y && y <= MAX_M;
}

// Point

// define Point's <
bool Point::operator <(const Point &o) const {
	return x < o.x || x == o.x && y < o.y;
}

// st03

st03::st03() {
	round = 0;
}

// return a random integer in [l, r]
int st03::intervalRandom(int l, int r) {
	return rand() % (r - l + 1) + l;
}

// return manhatton distance between (x, y) and my position
int st03::dist(int x, int y) {
	return abs(x - getX()) + abs(y - getY());
}

// assign Points
void st03::assignPoints() {
	while (getPoint() > 0) {
		int counter = 0, total = 0;
		for (int i = 1; i <= MAX_N; ++i)
			for (int j = 1; j <= MAX_M; ++j)
				if (askWhat(i, j) != EMPTY && askWhat(i, j) != FOOD) {
					total++;
					counter += sgn(1.5 * askHP(askWhat(i, j)), getMaxHP()) >= 0;
				}
		if (round < 200 && intervalRandom(0, 1000) >= 500 && int(counter * 1.7) >= total) {
			increaseHealth();
			continue;
		}
		if (round >= 200 && intervalRandom(0, 1000) >= 300 && int(counter * 2) >= total) {
			increaseHealth();
			continue;
		}

		if (intervalRandom(0, 1000) >= 300 && (getHP() <= getAtt() || getHP() <= getMaxHP() / 2)) {
			increaseHealth();
			continue;
		}

		if (getLevel() >= 5 && getSp() <= 13) {
			increaseSpeed();
			continue;
		}

		while (true) {
			int x = round <= 200 ? intervalRandom(0, 4) : intervalRandom(0, 5);
			if (x == 0) {
				if (sgn(getHP() * 1.5, 1. * getMaxHP()) <= 0 || getHP() < getAtt() * 3) {
					increaseHealth();
					break;
				}
			} else if (x == 1) {
				if (getSp() < MAX_SPEED) {
					increaseSpeed();
					break;
				}
			} else if (x == 2 || x == 3 || x == 4) {
				if ((round <= 10 || getLevel() <= 5 || getSp() < getAtt() * 2) && getSp() < MAX_SPEED)
					increaseSpeed();
				else
					increaseStrength();
				break;
			}
		}

		/*
		//get the priority of increaseHealth
		double rate = 1. * hp / maxHp;
		value[0] = pow(1. - (double)hp / maxHp, HP_KEY);

		//get the priority of increaseStrength
		value[1] = pow(1. - (double)att / MAX_ATTACK, ATTACK_KEY);

		//get the priority of increaseSpeed
		value[2] = pow(1. - (double)sp / MAX_SPEED, SPEED_KEY);

		for (int i = 0; i < 3; ++i)
			if (sgn(value[i], max(value[(i + 1) % 3], value[(i + 2) % 3])) >= 0) {
				if (i == 0) {
					increaseHealth();
				} else if (i == 1) {
					increaseStrength();
				} else if (i == 2) {
					increaseSpeed();
				}
				break;
			}
			*/
	}
}

// init
void st03::init() {
	for (int i = 0; i < 3; ++i)
		increaseHealth();
	for (int i = 0; i < 1; ++i)
		increaseStrength();
	for (int i = 0; i < 6; ++i)
		increaseSpeed();
	/*
	while (getPoint() > 0) {
		int x = intervalRandom(0, 2);
		if (x == 0) {
			increaseHealth();
		} else if (x == 1) {
			increaseStrength();
		} else if (x == 2) {
			increaseSpeed();
		}
	}
	*/
}

// revive
void st03::revive(int &x, int &y) {
	assignPoints();
	x = intervalRandom(1, MAX_N);
	y = intervalRandom(1, MAX_M);
}

// play
void st03::play() {
	round++;
	//move
	bool moved = false;
	Point choose[2];
	int delta;

	if (!moved && getHP() * 4 <= getMaxHP()) {
		vector<pair<int, pair<Point, Point> > > v;
		for (int x = 1; x <= MAX_N; ++x)
			for (int y = 1; y <= MAX_M; ++y)
				if ((askWhat(x, y) == EMPTY || x == getX() && y == getY()) && dist(x, y) <= getSp()) {
					for (int k = 0; k < 4; ++k) {
						int tx = x + MOVE[k][0];
						int ty = y + MOVE[k][1];
						if (!isValid(tx, ty))
							continue;
						int identity = askWhat(tx, ty);
						if (identity != FOOD)
							continue;
						int counter = 0;
						for (int r = 1; r <= MAX_N; ++r)
							for (int c = 1; c <= MAX_M; ++c)
								if (manhattonDist(Point(r, c), Point(x, y)) <= getSp())
									counter++;
						v.push_back(make_pair(counter, make_pair(Point(x, y), Point(tx, ty))));
					}
				}
		if ((int)v.size() > 0) {
			moved = true;
			sort(v.begin(), v.end());
			reverse(v.begin(), v.end());
			int x = intervalRandom(0, min(9, (int)v.size() - 1));
			move(v[x].second.first.x, v[x].second.first.y);
			attack(v[x].second.second.x, v[x].second.second.y);
		}
	}

	//try to find a position to kill a person with maximal blood
	if (!moved) {
		choose[0] = choose[1] = Point(INVALID, INVALID);
		delta = INF;
		for (int x = 1; x <= MAX_N; ++x)
			for (int y = 1; y <= MAX_M; ++y)
				if ((askWhat(x, y) == EMPTY || x == getX() && y == getY()) && dist(x, y) <= getSp())
					for (int k = 0; k < 4; ++k) {
						int tx = x + MOVE[k][0];
						int ty = y + MOVE[k][1];
						if (!isValid(tx, ty))
							continue;
						int identity = askWhat(tx, ty);
						if (identity == FOOD || identity == EMPTY)
							continue;
						int temp = getAtt() - askHP(identity);
						if (temp >= 0 && temp < delta) {
							delta = temp;
							choose[0] = Point(x, y);
							choose[1] = Point(tx, ty);
						}
					}
		if (delta != INF) {
			moved = true;
			move(choose[0].x, choose[0].y);
			attack(choose[1].x, choose[1].y);
		}
	}

	//try to eat algae
	if (!moved && (getHP() <= 10 && round <= 200 || intervalRandom(0, 1000) >= 300 || getHP() * 2 < getMaxHP())) {
		vector<pair<int, pair<Point, Point> > > v;
		for (int x = 1; x <= MAX_N; ++x)
			for (int y = 1; y <= MAX_M; ++y)
				if ((askWhat(x, y) == EMPTY || x == getX() && y == getY()) && dist(x, y) <= getSp()) {
					for (int k = 0; k < 4; ++k) {
						int tx = x + MOVE[k][0];
						int ty = y + MOVE[k][1];
						if (!isValid(tx, ty))
							continue;
						int identity = askWhat(tx, ty);
						if (identity != FOOD)
							continue;
						int counter = 0;
						for (int r = 1; r <= MAX_N; ++r)
							for (int c = 1; c <= MAX_M; ++c)
								if (manhattonDist(Point(r, c), Point(x, y)) <= getSp())
									counter++;
						v.push_back(make_pair(counter, make_pair(Point(x, y), Point(tx, ty))));
					}
				}
		if ((int)v.size() > 0) {
			moved = true;
			sort(v.begin(), v.end());
			reverse(v.begin(), v.end());
			int x = intervalRandom(0, min(9, (int)v.size() - 1));
			move(v[x].second.first.x, v[x].second.first.y);
			attack(v[x].second.second.x, v[x].second.second.y);
		}
		/* random choose
		vector<pair<Point, Point> > v;
		for (int x = 1; x <= MAX_N; ++x)
			for (int y = 1; y <= MAX_M; ++y)
				if ((askWhat(x, y) == EMPTY || x == getX() && y == getY()) && dist(x, y) <= getSp()) {
					for (int k = 0; k < 4; ++k) {
						int tx = x + MOVE[k][0];
						int ty = y + MOVE[k][1];
						if (!isValid(tx, ty))
							continue;
						int identity = askWhat(tx, ty);
						if (identity != FOOD)
							continue;
						v.push_back(make_pair(Point(x, y), Point(tx, ty)));
					}
				}
		if ((int)v.size() > 0) {
			moved = true;
			int x = intervalRandom(0, (int)v.size() - 1);
			move(v[x].first.x, v[x].first.y);
			attack(v[x].second.x, v[x].second.y);
		}
		*/
		/* random
		for (int x = 1; x <= MAX_N && !moved; ++x)
			for (int y = 1; y <= MAX_M && !moved; ++y)
				if ((askWhat(x, y) == EMPTY || x == getX() && y == getY()) && dist(x, y) <= getSp()) {
					for (int k = 0; k < 4; ++k) {
						int tx = x + MOVE[k][0];
						int ty = y + MOVE[k][1];
						if (!isValid(tx, ty))
							continue;
						int identity = askWhat(tx, ty);
						if (identity != FOOD)
							continue;
						moved = true;
						move(x, y);
						attack(tx, ty);
					}
				}
		*/
	}

	if (!moved) {
		//the blood is enough, kill them !!!
		if (getHP() >= KILL_BOUND || getHP() * 2 >= getMaxHP()) {
			vector<pair<int, pair<Point, Point> > > v;
			for (int x = 1; x <= MAX_N; ++x)
				for (int y = 1; y <= MAX_M; ++y)
					if ((askWhat(x, y) == EMPTY || x == getX() && y == getY()) && dist(x, y) <= getSp()) {
						for (int k = 0; k < 4; ++k) {
							int tx = x + MOVE[k][0];
							int ty = y + MOVE[k][1];
							if (!isValid(tx, ty))
								continue;
							int identity = askWhat(tx, ty);
							if (identity != FOOD && identity != EMPTY)
								v.push_back(make_pair(askHP(identity), make_pair(Point(x, y), Point(tx, ty))));
						}
					}
			if ((int)v.size() > 0) {
				moved = true;
				sort(v.begin(), v.end());
				reverse(v.begin(), v.end());
				int x = intervalRandom(0, min(10, (int)v.size() - 1));
				move(v[x].second.first.x, v[x].second.first.y);
				attack(v[x].second.second.x, v[x].second.second.y);
			}
		} 
		else if (intervalRandom(0, 1000) >= 300) { //find a place to hide myself >_<
			vector<pair<int, Point> > v;
			for (int x = 1; x <= MAX_N; ++x)
				for (int y = 1; y <= MAX_M; ++y)
					if ((askWhat(x, y) == EMPTY || x == getX() && y == getY()) && dist(x, y) <= getSp()) {
						int temp = 0;
						for (int k = 0; k < 4; ++k) {
							int tx = x + MOVE[k][0];
							int ty = y + MOVE[k][1];
							if (!isValid(tx, ty) || askWhat(tx, ty) == FOOD)
								temp++;
						}
						v.push_back(make_pair(4 - temp, Point(x, y)));
					}
			sort(v.begin(), v.end());
			if ((int)v.size() > 0) {
				moved = true;
				int x = intervalRandom(0, min((int)v.size(), HIDE_RANDOM_CONTROL) - 1);
				move(v[x].second.x, v[x].second.y);
			}
		}
	}

	//try to jump out
	if (!moved) {
		vector<Point> v;
		for (int x = 1; x <= MAX_N; ++x)
			for (int y = 1; y <= MAX_N; ++y)
				if ((askWhat(x, y) == EMPTY || x == getX() && y == getY()) && dist(x, y) <= getSp())
					v.push_back(Point(x, y));
		if ((int)v.size() > 0) {
			int x = intervalRandom(0, (int)v.size() - 1);
			move(v[x].x, v[x].y);
		}
	}

	assignPoints();
}
