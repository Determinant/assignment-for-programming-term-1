#include "env.h"
#include "ai.h"
#include "curses.h"
#include <cstdio>
#include <unistd.h>

Environment env;
char ch[52];
const int DELAY = 100000;
const int TOTAL_ROUND = 20;

void print(const int *order, int now) {
    int size = env.get_player_num();
    Map_t map = env.get_map();
    int id = order[now];
    for (int i = 0; i < MAP_MAXR; i++)
    {
        move(i, 0);
        for (int j = 0; j < MAP_MAXC; j++)
        {
            int t = map[i][j];
            if (t == PLACE_TYPE_BLANK) addch(' ');
            else if (t == PLACE_TYPE_FOOD) addch ('.');
            else 
            {

                if (-t == id)
                {
                    attron(COLOR_PAIR(1));
                    addch(ch[-t - 1]);
                    attroff(COLOR_PAIR(1));

                }
                else 
                    addch(ch[-t - 1]);
            }
        }
    }

    for (int i = 0; i < size; i++)
    {
        move(1, MAP_MAXC + 10 + i);
        FishInfo t = env.get_info(order[i]);
        if (t.status == PLAYER_STATUS_ALIVE)
        {
            attron(COLOR_PAIR(1));
            addch(ch[order[i] - 1]);
            attroff(COLOR_PAIR(1));
        }
        else 
            addch(ch[order[i] - 1]);
        move(0, MAP_MAXC + 10 + i);
        if (order[i] == id)
        {
            addch('v');
        }
        else
        {
            addch(' ');
        }
    }

    int r0 = 3, c0 = MAP_MAXC + 10;
    int col_width = 10, spacing = 2;

    move(2, c0 + col_width * 1 + spacing * 0);
    printw("HP/maxHP");

    move(2, c0 + col_width * 2 + spacing * 1);
    printw("Speed");

    move(2, c0 + col_width * 3 + spacing * 2);
    printw("Strength");

    move(2, c0 + col_width * 4 + spacing * 3);
    printw("Point");

    move(2, c0 + col_width * 5 + spacing * 4);
    printw("Level");

    move(2, c0 + col_width * 6 + spacing * 5);
    printw("Exp");

    move(2, c0 + col_width * 7 + spacing * 6);
    printw("Score");

    for (int i = 1; i <= size; i++)
    {
        FishInfo ret = env.get_info(i);
        static char buff[1024];
        move(r0 + i - 1, c0);
        addch(ch[i - 1]);


        sprintf(buff, "%d/%d\n", ret.cur_health, ret.max_health);
        move(r0 + i - 1, c0 + col_width * 1 + spacing * 0);
        printw(buff);

        sprintf(buff, "%d\n", ret.speed);
        move(r0 + i - 1, c0 + col_width * 2 + spacing * 1);
        printw(buff);

        sprintf(buff, "%d\n", ret.strength);
        move(r0 + i - 1, c0 + col_width * 3 + spacing * 2);
        printw(buff);


        sprintf(buff, "%d\n", ret.point);
        move(r0 + i - 1, c0 + col_width * 4 + spacing * 3);
        printw(buff);


        sprintf(buff, "%d\n", ret.level);
        move(r0 + i - 1, c0 + col_width * 5 + spacing * 4);
        printw(buff);


        sprintf(buff, "%d\n", ret.exp);
        move(r0 + i - 1, c0 + col_width * 6 + spacing * 5);
        printw(buff);

        sprintf(buff, "%d\n", ret.score);
        move(r0 + i - 1, c0 + col_width * 7 + spacing * 6);
        printw(buff);
    }

    refresh();
    usleep(DELAY);

}

void print_level_up(int id) {
    FishInfo info = env.get_info(id);
    move(info.pos_x, info.pos_y);
    attron(COLOR_PAIR(2));
    addch(ch[id - 1]);
    attroff(COLOR_PAIR(2));
    refresh();
    usleep(DELAY);
}

void print_attack(int id, int x, int y, int flag) {
    if (flag == PLACE_TYPE_FOOD)
    {
        FishInfo info = env.get_info(id);
        move(x, y);
        attron(COLOR_PAIR(3));
        addch('*');
        attroff(COLOR_PAIR(3));
        refresh();
        usleep(DELAY);

    }
}

int main() {

    for (int i = 0; i < 26; i++)
    {
        ch[i] = i + 'A';
        ch[26 + i] = i + 'a';
    }

#ifdef ONLY_RESULT
    env.add_player(new st01);
    env.add_player(new st02);
    env.add_player(new st03);
    env.game_start();

    curs_set(0);
    start_color();          
    init_pair(1, COLOR_BLACK, COLOR_WHITE);
    init_pair(2, COLOR_WHITE, COLOR_BLUE);
    init_pair(3, COLOR_WHITE, COLOR_RED);
    while (env.get_turn_num() < TOTAL_ROUND)
    {
        env.turn();
        fprintf(stderr, "%d\n", env.get_turn_num());
        usleep(DELAY);
    }

    initscr();
    env.set_callback_before(print);
    env.set_callback_level_up(print_level_up);
    env.set_callback_attack(print_attack);
    env.turn();

    getch();
    endwin();
#else

    env.add_player(new st01);
    env.add_player(new st02);
    env.add_player(new st03);

    env.set_callback_before(print);
    env.set_callback_level_up(print_level_up);
    env.set_callback_attack(print_attack);

    env.game_start();
    initscr();

    curs_set(0);
    start_color();          
    init_pair(1, COLOR_BLACK, COLOR_WHITE);
    init_pair(2, COLOR_WHITE, COLOR_BLUE);
    init_pair(3, COLOR_WHITE, COLOR_RED);
    while (env.get_turn_num() <= TOTAL_ROUND)
    {
        env.turn();
        usleep(DELAY);
    }

    getch();
    endwin();
#endif
}
