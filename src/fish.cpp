#include "env.h"
#include "fish.h"

#include <algorithm>

using std::abs;
typedef fish Fish; // redefine the ugly name

class Environment;
class FishMeta;

void Fish::setID(int id_) {
    fmptr -> id = id_;
} // set the identity number

bool Fish::move(int new_x, int new_y) {
    return fmptr -> move(new_x - 1, new_y - 1);
}

bool Fish::attack(int des_x, int des_y) {
    return fmptr -> attack(des_x - 1, des_y - 1);
}

int Fish::getPoint() {
    return fmptr -> point;
}

int Fish::getLevel() {
    return fmptr -> level;
}

int Fish::getExp() {
    return fmptr -> exp;
}

int Fish::getX() {
    return fmptr -> pos_x + 1;
}

int Fish::getY() {
    return fmptr -> pos_y + 1;
}

int Fish::getHP() {
    return fmptr -> cur_health;
}

int Fish::getMaxHP() {
    return fmptr -> max_health;
}

int Fish::getAtt() {
    return fmptr -> strength;
}

int Fish::getSp() {
    return fmptr -> speed;
}

int Fish::getID() {
    return fmptr -> id;
}

int Fish::getTotalPlayer() {
    return fmptr -> get_total_player();
}

int Fish::askWhat(int x, int y) {
    return fmptr -> get_content(x - 1, y - 1);
}

int Fish::askHP(int id) {
    return fmptr -> get_hp(id);
}

bool Fish::increaseHealth() {
    return fmptr -> increase_health();
}

bool Fish::increaseStrength() {
    return fmptr -> increase_strength();
}

bool Fish::increaseSpeed() {
    return fmptr -> increase_speed();
}

