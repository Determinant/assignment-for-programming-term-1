#! /bin/bash -e
cp ai_makefile_template AIs/Makefile
cd AIs/
printf "all: " >> Makefile
cat < /dev/null > ../src/ai.h
for i in *.cpp
do
    name=$(echo "$i" | sed "s/cpp//g")
    echo "Found AI: $name"
    obj="$name""o"
    header="$name""h"
    printf "$obj " >> Makefile
        echo "#include \"../AIs/$header\"" >> ../src/ai.h
done
